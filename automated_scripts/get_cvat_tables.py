#!/usr/bin/env python3
# Creates a user and/or jobs csv file
import argparse
import csv
import json
import os
import pandas as pd
import requests as rq
import yaml

parser = argparse.ArgumentParser(
    prog="Get Cvat Tables v1.0.0",
    description="Saves the Cvat jobs and user info into structured tables (csv)",
)

parser.add_argument(
    "--quiet",
    action="store_true",
    help="Silences all debug output",
)

parser.add_argument(
    "--users",
    type=str,
    action="store",
    metavar="<save-file>",
    help="Output file name for users table. It'll be a csv file.",
)

parser.add_argument(
    "--jobs",
    type=str,
    action="store",
    metavar="<save-file>",
    help="Output file name for jobs table. It'll be a csv file.",
)

args = parser.parse_args()

if args.users is None and args.jobs is None:
    print("ERROR: Supply one of --users or --jobs")
    exit(1)


# Load config >>>>
if os.path.exists("config.yml"):
    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
else:
    with open("../config.yml", "r") as f:
        config = yaml.safe_load(f)

auth = rq.auth.HTTPBasicAuth(
    config["authentication"]["username"],
    config["authentication"]["password"],
)


def api_url(path):
    c = config["connection"]
    return f"{c['protocol']}://{c['domain']}:{c['port']}/api/{path}"


# <<<<<<<<


class User:
    columns = [
        "id",
        "username",
        "first_name",
        "last_name",
        "completed_frames",
        "pending_validation_frames",
        "not_done_frames",
        "assigned_frames",
    ]

    def __init__(self, user_id):
        self.data = {x: None for x in self.columns}
        self.data["id"] = user_id

    def to_csv_row(self):
        return [self.data[column] for column in self.columns]


class Job:
    columns = [
        "id",
        "task_id",
        "stage",
        "state",
        "frame_count",
        "assignee_id",
        "assignee_username",
        "project_id",
        "project_name",
    ]

    def __init__(self, job_id):
        self.data = {x: None for x in self.columns}
        self.data["id"] = job_id

    def to_row(self):
        return [self.data[column] for column in self.columns]


# Download the job table (required in either case)
df_jobs = pd.DataFrame([], columns=Job.columns)
if not args.quiet:
    print("Downloading jobs...")

for i in range(1, 10**8):
    response = rq.get(api_url(f"jobs?page={i}"), auth=auth)
    assert response.status_code == 200, "Successful response from cvat API"

    reply = response.json()
    jobs = reply["results"]

    for job in jobs:
        obj = Job(job["id"])

        obj.data["task_id"] = job["task_id"]
        obj.data["stage"] = job["stage"]
        obj.data["state"] = job["state"]
        obj.data["frame_count"] = job["stop_frame"] - job["start_frame"] + 1
        obj.data["project_id"] = job["project_id"]

        if job["assignee"] is not None:
            obj.data["assignee_id"] = job["assignee"]["id"]
            obj.data["assignee_username"] = job["assignee"]["username"]

        df_jobs.loc[len(df_jobs)] = obj.to_row()

    if reply["next"] is None:
        break

# Get all the project names
df_projects = pd.DataFrame([], columns=["pid", "pname"])
project_names = dict()

for i in range(1, 10**8):
    response = rq.get(api_url(f"projects?page={i}"), auth=auth)
    assert response.status_code == 200, "Successful response from cvat API"

    reply = response.json()

    for project in reply["results"]:
        df_projects.loc[len(df_projects)] = [project["id"], project["name"]]

    if reply["next"] is None:
        break

df_jobs = df_jobs.merge(df_projects, left_on="project_id", right_on="pid", how="inner")
df_jobs.drop(columns=["project_name", "pid"], inplace=True)
df_jobs.rename(columns={"pname": "project_name"}, inplace=True)

# Download user table
df_users = pd.DataFrame(
    [],
    columns=[
        "id",
        "username",
        "first_name",
        "last_name",
        "completed_frames",
        "pending_validation_frames",
        "not_done_frames",
        "assigned_frames",
    ],
)
if not args.quiet:
    print("Downloading user table...")

for i in range(1, 10**8):
    response = rq.get(api_url(f"users?page={i}"), auth=auth)
    assert response.status_code == 200, "Successful response from cvat API"

    reply = response.json()

    for u in reply["results"]:
        udf = df_jobs[df_jobs["assignee_id"] == u["id"]]

        df_users.loc[len(df_users)] = [
            u["id"],
            u["username"],
            u["first_name"],
            u["last_name"],
            udf[udf["stage"] == "acceptance"]["frame_count"].sum(),
            udf[udf["stage"] == "validation"]["frame_count"].sum(),
            udf[udf["stage"] == "annotation"]["frame_count"].sum(),
            udf["frame_count"].sum(),
        ]

    if reply["next"] is None:
        break

if args.users is not None:
    df_users.to_csv(args.users, index=False)

if args.jobs is not None:
    df_jobs.to_csv(args.jobs, index=False)
