#!/usr/bin/env python3
# Writes a csv file of basic user information
import csv
import json
import requests as rq
import yaml

# Load config >>>>
if os.path.exists("config.yml"):
    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
else:
    with open("../config.yml", "r") as f:
        config = yaml.safe_load(f)


def api_url(path):
    c = config["connection"]
    return f"{c['protocol']}://{c['domain']}:{c['port']}/api/{path}"


login_resp = rq.post(api_url("auth/login"), json=config["authentication"])
cookies = login_resp.cookies
# <<<<<<<<

response = rq.get(api_url(f"server/annotation/formats"), cookies=cookies)
assert response.status_code == 200, "Successful response from cvat API"

results = response.json()["exporters"]

for result in results:
    print(result["name"])
