#!/usr/bin/env python3
import os
import pandas as pd
import subprocess
import sys
import tempfile
from tqdm import tqdm

# Make sure we're in the TLD
if ".git" not in os.listdir():
    os.chdir("..")

assert ".git" in os.listdir(), "Not at top-level git directory"

# Grab jobs table
_, jobs_file = tempfile.mkstemp()

subprocess.run(
    [
        sys.executable,
        "automated_scripts/get_cvat_tables.py",
        "--jobs",
        jobs_file,
    ],
    check=True,
)

# Update completed validation to completed acceptance
df = pd.read_csv(jobs_file).set_index("id")
os.remove(jobs_file)

to_complete = list(
    df[(df["stage"] == "validation") & (df["state"] == "completed")].index
)

for jid in tqdm(to_complete):
    subprocess.run(
        [
            sys.executable,
            "automated_scripts/update_job.py",
            "--stage", "acceptance",
            "--state", "completed",
            str(jid),
        ],
        check=True,
    )
