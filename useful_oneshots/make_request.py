#!/usr/bin/env python3
# Runs a single request to cvat. Mostly for debugging
import argparse
import json
import os
import requests as rq
import tempfile
import yaml

parser = argparse.ArgumentParser(
    prog="Make Cvat Request v1.0.0",
    description="Runs a single cvat request, mostly for debugging",
)

parser.add_argument(
    "--type",
    type=str,
    action="store",
    metavar="<REST-TYPE>",
    default="GET",
    help="Type of request being made",
)

parser.add_argument(
    "request",
    type=str,
    action="store",
    metavar="<request>",
    help="Request string you'd like to run",
)

args = parser.parse_args()

# Load config >>>>
if os.path.exists("config.yml"):
    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
else:
    with open("../config.yml", "r") as f:
        config = yaml.safe_load(f)

auth = rq.auth.HTTPBasicAuth(
    config["authentication"]["username"],
    config["authentication"]["password"],
)


def api_url(path):
    c = config["connection"]
    return f"{c['protocol']}://{c['domain']}:{c['port']}/api/{path}"
# <<<<<<<<

args.type = args.type.upper()

if not args.request.startswith("/"):
    args.request = "/" + args.request

if args.request.startswith("/api"):
    args.request = args.request[4:]

args.request = args.request[1:]

print(f"Making ({args.type}) request: `{api_url(args.request)}`")

if args.type == "GET":
    response = rq.get(api_url(args.request), auth=auth)
elif args.type == "POST":
    response = rq.post(api_url(args.request), auth=auth)
elif args.type == "PATCH":
    response = rq.patch(api_url(args.request), auth=auth)
elif args.type == "PUT":
    response = rq.put(api_url(args.request), auth=auth)
elif args.type == "DEL":
    response = rq.delete(api_url(args.request), auth=auth)
else:
    print(f"Unsupported request type: {args.type}")
    exit(1)

print(f"RESPONSE CODE: {response.status_code}")
reply = response.json()
print(json.dumps(reply, indent=4))

_, json_file = tempfile.mkstemp()
with open(json_file, 'w') as f:
    json.dump(reply, f)

print(f"Json also availble at: {json_file}")
print(f"RESPONSE CODE: {response.status_code}")
