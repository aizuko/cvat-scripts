# CVAT scripts

This repository contains a few automation scripts for managing cvat. They're
particularly useful if you're tunin a YOLO model, as it'll build the 
directory structure and shuffle the data as well.

We use these scripts at [ARVP](https://arvp.org) to easily manage a team of
about 30 labeling members and reduce the data collection steps for training.

## Setup

Start by copying `template.config.yml` to `config.yml`. Fill in the fields
with an admin user's credentials. If your `CVAT_HOST` variable uses a 
different domain in your `docker-compose.yml`, update the domain and protocol
as well.

## Usage

```bash
./interactive_scripts/download_jobs.py
```

The current inventory includes:

 - A user-info script
 - A job-info script
 - Job-downloading script (to be used in conjunction with job info script)

In addition to the automation scripts, some interactive convenience scripts
assist with common downloads

### Using Google Sheets

Most exports (except images) are saved as csv files. If you plan to import 
these into Google Sheets, click `file` -> Import -> Upload -> Replace Current 
Sheet.
